### GIT REPOSITORY CLONE: ###
-----------------------------
* Clone: Run this command on your /var/www/html/ directory git@bitbucket.org:EmayavarambanP_Fuel/coachlist.git folder_name
* Update .env file with your server, admin_email and database credentials
* Run this command "php artisan migrate" to migrate tables;
* We have created virtual host for this project, modify it as mentioned below :
	- i)   sudo nano /etc/apache2/sites-enabled/000-default.conf
	- ii)  Change the document root "DocumentRoot /var/www/html/coachlist/public"
	- iii) Provide the directory permissions 
		<Directory /var/www/html/coachlist/public>
            AllowOverride All
            Require all granted
        </Directory>
	- iv)  sudo nano /var/www/html/coachlist/public/.htaccess
	- v) Add RewriteBase /var/www/html/coachlist/public
* After finshing the setup, run the website url on your browser EX:"http://dev.businessfuelit.in/"