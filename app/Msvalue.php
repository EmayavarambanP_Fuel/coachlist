<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Msvalue extends Model
{
	//table name
    protected $table = 'MSValues';
    //required field
    protected $fillable = ['MSValues'];   
}
