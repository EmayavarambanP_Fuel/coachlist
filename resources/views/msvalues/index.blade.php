@extends('layouts.app')
@section('content')
<!--Header section starts here -->
  <div class="navbar">
    <a class="brand-logo">Coachlist</a>
    <ul id="nav-menu" class="right">
      @if(Auth::check())
        <li><a>{{ Auth::user()->name }}</a></li>
        <li><a href="/users/logout">Logout</a></li>
       @endif
      
    </ul>
  </div>
<!--Header section ends here -->    
  <div class="cl-container-2">
    <!--Msvalue form success message starts here--> 
    @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
    @endif
    <!--Msvalue form success message ends here--> 
    <!--Msvalue form Error message starts here-->
    @if($errors->has())
      @foreach ($errors->all() as $error)
        <div>{{ $error }}</div>
      @endforeach
    @endif
    <!--Msvalue form Error message ends here-->
    <!--Msvalue form starts here-->
    <form action="/save" method="POST">
      {{ csrf_field() }}
      <div class="input-group">
          <label class="cl-label">MsValue</label>
          <input name="MSValues" type="text" class="cl-input" placeholder="Enter your value here..."/>
      </div>
      <input type="submit" class="cl-cta-btn" value="Save"/>
    </form>
    <!--Msvalue form ends here-->
  </div>
   @endsection